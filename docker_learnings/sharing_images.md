# Sharing images & containers

Thanks to docker, that we need not bother installing/re-installing our application dependencies like node or python, globally on our system, as it's already there in the image and thus container based on the image.

This allows us to have multiple projects on our system, with different dependencies and different versions, and these projects could co-existing side by side, without us having to bother to uninstall or re-install with different versions of the application dependencies. For example, we could have a python-3.7 based application, next to a python-3.9 based application, without switching versions of python globally in our system.

Even if that would be all we're using Docker for, that would already be great. But still we got even more reasons from using Docker. We use docker to be able to share our application images and container with others, which could be other team members or also be with the servers on which we want to deploy our containers in the end.

Coming to sharing images and containers, we need not share containers but just the image alone would do because anyone could run a container based on the image we share.

Typically we have 2 ways of sharing images:

1. Sharing the application source-code along with the `Dockerfile`
2. Sharing a built image
   - Besides sharing a `Dockerfile` along with all of the surrounding application source-code, and leaving it upto the recipient to build an image based on it, we can share an already complete built image
   - Getting a built image, would simply leave us just to download the image and use the `FROM` instruction or running a container instantly based on the downloaded image, without bother about the surrounding application source-code, because it would already be included in the built image.

## Sharing built images

- We don't need to look for actual files of the built images and thus send those around.
- Docker has built-in commands for sharing images and using shared images, like `docker push` and `docker pull`
- We have two main places to share the built images

  - Docker Hub (official docker image registry)
  - Any other private docker image registry

## Pusing & pulling built images to & from Docker hub

- create a repository for the image in docker hub

  - build an image with same name as of the repository, for example
    ```shell
    $ docker build -t vishalgovind/node-hello-world /node-app
    ```
  - now, push the built image using `docker push` command
    ```shell
    $ docker push vishalgovind/node-hello-world
    ```
    `docker push` command by default uses "latest" as the tag part if not specified, for the built image, while pushing.
  - running this might give access denied error, if we are not logged in locally in our system with docker hub.

    ```shell
      $ docker push vishalgovind/node-hello-world
      Using default tag: latest
      The push refers to repository [docker.io/vishalgovind/node-hello-world]
      90f5c274097d: Preparing
      f258ea862c85: Preparing
      d358054f0a76: Preparing
      44961b599b5f: Preparing
      99f6e31e2c22: Preparing
      b305b22e4755: Waiting
      912ae57dfff4: Waiting
      1ce553a2c417: Waiting
      277f5e9c3e54: Waiting
      ded505685899: Waiting
      b3cde1dc88f9: Waiting
      b57940a08efc: Waiting
      c084b22a2a9b: Waiting
      denied: requested access to the resource is denied
    ```

    - We should first be logged in from our system to docker hub

      ```shell
      $ docker login
      Login with your Docker ID to push and pull images from Docker Hub. If you dont have a Docker ID, head over to https://hub.docker.com to create one.
      Username: vishalgovind
      Password:
      Login Succeeded

      Logging in with your password grants your terminal complete access to your account.
      For better security, log in with a limited-privilege personal access token. Learn more at https://docs.docker.com/go/access-tokens/
      ```

      - We can also logout from Docker Hub from our local system using `docker logout` command

        ```shell
        $ docker logout
        ```

- Now we can pull the pushed image even if we are not logged in locally using Docker hub

  ```shell
  $ docker logout
    Removing login credentials for https://index.docker.io/v1/
  $ docker pull vishalgovind/node-hello-world
    Using default tag: latest
    latest: Pulling from vishalgovind/node-hello-world
    fac7262b6510: Already exists
    38880e907cdc: Already exists
    a8181793414c: Already exists
    43bb9401c1ad: Already exists
    8d8786170cdb: Already exists
    74bb356e602f: Already exists
    339df40e9d31: Already exists
    95d94dc8b52a: Already exists
    39293db10479: Already exists
    df4da2758c11: Already exists
    1fdace5c9580: Already exists
    26c21415a3fd: Already exists
    b74e7265f4e4: Already exists
    Digest: sha256:e2f77f6ed87abb98c9e874097c3cafe9d65a5ed16a78a6efa3f3b264e434846b
    Status: Downloaded newer image for vishalgovind/node-hello-world:latest
    docker.io/vishalgovind/node-hello-world:latest
  ```

- `docker pull` always fetches latest image from the image registry.
  - Thus in the mean time if there are any pushes made to the image registery, `docker pull` will fetch the latest image.
  - `docker run` when run without pulling, for the first time (without ever pulling that image before), it will pull latest image and run a container based on it.
  - `docker run` won't automatically check for any updates on the image in the image registry, if the container is being built on an image which is already cached in the local system.
    - In such cases, first run `docker pull` prior to running `docker run`.
