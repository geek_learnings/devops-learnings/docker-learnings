# Volumes

- Our applications usually generate a lot of data while running. For example, the application allows users to upload files, while are thus data generated dynamically while running. We would want to make such data available to all the running instances/containers of the application-image.

- Although containers can read/write data inside the container, while running, such data is lost once the container is removed, if the data written is inside the container itself.

- We want the container to write such application-generated data in such a storage location, where we would be able to make it available to all the containers which would ever run based on the same image. Volumes let us achieve the same.

- Volumes are storage locations which are present inside the localhost machine, rather than inside of the containers. Containers are attach to these volumes, to refer for any data storage in them.

- Storing data in volumes even allows to avoid storing everything inside the container, and thus improving performance and efficiency of the container.

## Named and Anonymous Volumes

- There are 2 types of volumes: named-volumes & anonymous-volumes

### Named volumes

- Survive container restarts or removals

- More than one container can use the same named-volume, thus allowing to achieve sharing of application generated data, across containers or even container restarts/re-run after removal

- These volumes are present somewhere in the localhost machine, which are hard to find.

- docker command

  ```shell
  $ docker run -v feedback:/app/feedback --name feedback-app feedback-image
  ```

### Anonymous vollumes

- Survive container restarts but not removals

- Bind to only one container which attaches it. Thus, cannot share the data stored inside such volumes

- docker command

  ```shell
  $ docker run -v /app/temp --name feedback-app feedback-image
  ```

## Volume overrides

- Every volume is linked to certain path inside the container.

- The data stored in the volume is the data which application would want to store inside certain path of the container.

- For instance, consider the following folder structure of a container,

  ```shell
  . ------------------------> v1
  ├── Dockerfile
  ├── feedback -------------> v2
  ├── package.json
  ├── pages
  │   ├── exists.html
  │   └── feedback.html
  ├── public
  │   └── styles.css
  ├── server.js
  └── temp -----------------> v3
      ├── awsome.txt
      └── test.txt
  ```

- v1, v2 and v3 are volumes which are mapped to the corresponding path in the container.

- The volume v1 stores the entire container folder, but since v2 and v3 are volumes which point to paths which are deeper than the path pointed to by v1, they override volume v1. i.e., v1 will not keep track of data stored in paths pointed to by v2 and v3, because they override v1, reason being that the path pointed by v2 and v3 is at a deeper level than the path pointed to by v1. Thus, any writing of data inside the paths pointed to by v2 and v3 will not be stored inside v1.

- The container will refer v2 or v3 whenever it has something to deal with data inside paths pointed to by v2 or v3.

# Bind mounts (sync changes in host with running container)

- These are not volumes, but similar to named volumes when it comes to the command used to create them.

- These are prefered to be used only during development, to be able to setup a live-connection between the application source code and the running container, which is capable of listening to any changes to the source code even while the container is running, and thus sync the container and the host-machine-source-code incase of any changes in the source code while the container is running.

- A bind-mount is also mapped to certain path inside the container and also the host machine and connects both the paths to keep such paths in sync while the container is running, to be able to sync any changes either side.

  ```shell
  $ docker run -v $(pwd):/app --name feedback-app feedback-image
  ```

- It can also be configured as read-only(ro) bind-mount, to avoid any changes from within container and only allow changes from the host-machine by the developer.

  ```shell
  $ docker run -v $(pwd):/app:ro --name feedback-app feedback-image
  ```

- When a bind-mount mapped to a certain path is created, the bind-mount will override the data stored in that path inside the container with the data stored in the same path in the host-machine, even if during building of the image, there is some data generated in that path.

- For instance, we generate `node_modules` folder when we build an image for a node project. Thus, if we setup a bind-mount pointing to a path which is above the `node_modules` folder, the data in the `node_modules` folder in the running container will be overriden with the data in the `node_modules` folder in the host machine.

- To avoid this, we could configure an anonymous volume point to `node_modules` folder in the container which would then override the bind-mount mapping anywhere above the node_modules folder in the container

  ```shell
  $ docker run -v $(pwd):/app -v /app/node_modules --name feedback-app feedback-image
  ```

# Docker CLI for Volumes and Bind-mounts

- `docker volume ls` - list all created volumes
- `docker volume create` - creates a named volume
- `docker volume rm <vol-name>` - removes a volume
- `docker volume prune` - removes all unused volumes
- `docker run -v feedback:/path/in/container` - creates a container and attaches a named volume named "feedback" and maps to the path mentioned after ":" from inside the container.
- `docker run -v /path/in/container` - creates a container and attaches an anonymouse volume and maps to the path mentioned after ":" from inside the container.
- `docker run -v /path/in/host/machine:/path/in/container` - creates a container and attaches an bind-mount and maps to the path mentioned after ":" from inside the container and thus listens to any changes in the path in host and syncs the same in the path in container, and vice-versa.
- `docker run -v /path/in/host/machine:/path/in/container:ro` - creates a container and attaches an bind-mount in read-only and maps to the path mentioned after ":" from inside the container and thus only listens to any changes in the path in host and syncs the same in the path in container, and not vice-versa.
