# Docker CLI

- Use `docker --help` to get a list of all commands supported by the docker CLI

## `docker ps`

- List all containers currently running: `docker ps`
  ```shell
  CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
  ```
- List all containers running: `docker ps -a`
  ```shell
  CONTAINER ID   IMAGE     COMMAND                  CREATED       STATUS                   PORTS     NAMES
  aa41b0c15c7a   node      "docker-entrypoint.s…"   4 weeks ago   Exited (0) 4 weeks ago             boring_cohen
  0b202e8f0e11   node      "docker-entrypoint.s…"   4 weeks ago   Exited (0) 4 weeks ago             adoring_blackwell
  eb163a34fde9   node      "docker-entrypoint.s…"   4 weeks ago   Exited (0) 4 weeks ago             eager_snyder
  8d47854f3eae   node      "docker-entrypoint.s…"   4 weeks ago   Exited (0) 4 weeks ago             boring_edison
  ```

## `docker images`

- List all images
  ```shell
  $ docker images
  ```

## `docker start`

- With every `docker run <image>`, we create a new container based on the \<image>. But to restart an existing container:

  ```shell
  $ docker start <container-id/name>
  ```

## `docker stop`

- To stop container

  ```shell
  $ docker stop <container-id/name>
  ```

### Attached VS detached mode of running containers

- When a container runs in attached mode, the terminal process is not exited, whereas while running a container in detached mode, the terminal process is exited and the container still runs in the foreground
- for running new containers using `docker run <image-id/name>`, attached-mode is by default used
- for restarting existing containers using `docker start <container-id/name>`, detached-mode is by default used
- for any of these commands, we could configure to run in mode different from the one used by default
- We would want to run in attached mode when we want to listen to the output from the application ran by the container, like whenever something gets logged to console from within the application, if we want that log, we would want to run in attached mode

  ### `docker run -d` (running new container in detached mode)

  ```
  $ docker run -d <image-id/name>
  ```

  ### `docker start -a` (restarting stopped container in attached mode)

  ```
  $ docker start -a <image-id/name>
  ```

- detached mode exits the terminal process and returns the newly created and foreground-running container-id

## `docker attach`

- to listen to a container already running in detached mode, we could use
  ```shell
  $ docker attach <container-id/name>
  ```

## `docker logs`

- fetches the logs which were printed by the container so far

  ```shell
  $ docker log <container-id/name>
  ```

  ### `docker logs` follow mode

  - to fetch the logs printed so far by the container and also keep on listening hence forth
    ```shell
    $ docker log -f <container-id/name>
    ```

## Interacting with application running in the container

- Even while running a container in attached mode, we are able to listen to output from the application running in the container, but we cannot interact with application if it asks for any input from the user, i.e. we cannot pass any command-line inputs to the application if it tries to ask any from the user, even in the attached mode of running the container
- To achieve this interaction with the application running in the container, use
  ```shell
  $ docker run -i -t <image-id/name>
  ```
  for running existing container in interactive mode
  ```shell
  $ docker start -i <container-id/name>
  ```

## Removing images & containers

- ## `docker rm` (to remove stopped container)

  - It is not possible to remove currently running containers
  - Removing stopped containers:
    ```shell
    $ docker rm <container-id/name-1> <container-id/name-2> <container-id/name-3> ...
    ```

- ## `docker rmi` (to remove images with no containers (be it running or stopped))

  - to delete an image, there should not be any containers based on it, be it stopped or running. All the containers (running or stopped) based on the image need to be removed first.
  - it deletes the image along with all of its layers

  ```shell
    $ docker rmi <image-id/name-1> <image-id/name-2> <image-id/name-3> ...
  ```

- ## `docker image prune` (to remove all unused images w/o tags)

  ```shell
  $ docker image prune
  ```

  - `docker image prune -a` (to remove all unused images w/ or w/o tags)

    ```shell
    $ docker image prune
    ```

- ## `docker run --rm` (automatically remove container when stopped)
  - using --rm while running a container, based on an image, will automatically remove the container when it is stopped
  ```shell
  $ docker run --rm <image-id/name>
  ```

## Inspecting information about an image

- ## `docker image inspect`

  - To get all information an image, use:

    ```shell
      $ docker image inspect <image-id/name>
      [
        {
          "Id": "sha256:6bf8dd2606f783e80526cc5532190164e261bee95a6fd6e9d534f1ff8c83fdab",
          "RepoTags": [],
          "RepoDigests": [],
          "Parent": "",
          "Comment": "buildkit.dockerfile.v0",
          "Created": "2023-02-02T16:53:42.891240217Z",
          "Container": "",
          "ContainerConfig": {
              "Hostname": "",
              "Domainname": "",
              "User": "",
              "AttachStdin": false,
              "AttachStdout": false,
              "AttachStderr": false,
              "Tty": false,
              "OpenStdin": false,
              "StdinOnce": false,
              "Env": null,
              "Cmd": null,
              "Image": "",
              "Volumes": null,
              "WorkingDir": "",
              "Entrypoint": null,
              "OnBuild": null,
              "Labels": null
          },
          "DockerVersion": "",
          "Author": "",
          "Config": {
              "Hostname": "",
              "Domainname": "",
              "User": "",
              "AttachStdin": false,
              "AttachStdout": false,
              "AttachStderr": false,
              "ExposedPorts": {
                  "3000/tcp": {}
              },
              "Tty": false,
              "OpenStdin": false,
              "StdinOnce": false,
              "Env": [
                  "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                  "NODE_VERSION=14.21.2",
                  "YARN_VERSION=1.22.19"
              ],
              "Cmd": [
                  "node",
                  "app.mjs"
              ],
              "ArgsEscaped": true,
              "Image": "",
              "Volumes": null,
              "WorkingDir": "/app",
              "Entrypoint": [
                  "docker-entrypoint.sh"
              ],
              "OnBuild": null,
              "Labels": null
          },
          "Architecture": "arm64",
          "Variant": "v8",
          "Os": "linux",
          "Size": 866341237,
          "VirtualSize": 866341237,
          "GraphDriver": {
              "Data": {
                  "LowerDir": "/var/lib/docker/overlay2/n0bh2svhtv1zhhaaokxkezc3m/diff:/var/lib/docker/overlay2/jb77xcrqtmu7pmc6th26yate9/diff:/var/lib/docker/overlay2/puhz50ydtl1e7nyh00ujk0x3y/diff:/var/lib/docker/overlay2/8744b9188e0adcac9b472ae50736e3c0638917a661022d0d4066491f2ce5d664/diff:/var/lib/docker/overlay2/8006825821c45593ad7f10035e18ec44d36a1807f20d5eee85cecd000287df38/diff:/var/lib/docker/overlay2/2d50a298138eae467bf20dbc5fb8e6b5ee1457c744d1841a69061e9d8596befe/diff:/var/lib/docker/overlay2/9728259e06b7bf9438751c05e5eb123ff5807ed08d1e0cccdd9ac519a926d294/diff:/var/lib/docker/overlay2/f418953bdcce2656e18473d2f8e3d0ca1dcb359fb0affdaf02df654c6d726cfe/diff:/var/lib/docker/overlay2/4f6af330457e55b9188c6f3453ae0222874dc2458afe24af2aae4d08d6d3e833/diff:/var/lib/docker/overlay2/89f311f62be12c1e49edf716a3852a428acd17b399fe0dbdd264d03aede707ae/diff:/var/lib/docker/overlay2/afc989c363b436784e36425f4fd09c67e821291c3d2af6a30802f28fe0d45732/diff:/var/lib/docker/overlay2/c7fef2596c84895e565634fb0be3d388f3b22917382789c76f21faaef1fa2b11/diff",
                  "MergedDir": "/var/lib/docker/overlay2/kzhf1f110rbxut5sng6xs52cz/merged",
                  "UpperDir": "/var/lib/docker/overlay2/kzhf1f110rbxut5sng6xs52cz/diff",
                  "WorkDir": "/var/lib/docker/overlay2/kzhf1f110rbxut5sng6xs52cz/work"
              },
              "Name": "overlay2"
          },
          "RootFS": {
              "Type": "layers",
              "Layers": [
                  "sha256:9586e1383a50bb85bb682e52ad9fdce431f1879dd2f9c5627323f7b47b92a9bd",
                  "sha256:2246c544f6d9b5376ea1f89e172c762c8b3285e9206ec27b3c5332b486dc8e56",
                  "sha256:18488952d1ef01fbc8c27a5f75e299f7d58f37801e530a8537b6674101a46f80",
                  "sha256:23fddaecc2ccf3cc2d5e324128791320aeb0da760fc94fab413c18b20c2a73b7",
                  "sha256:6886d57135d8a13571aaf65f9137bcd02141c3cb599c3fb9df3342e28d5a4225",
                  "sha256:f947751b45d25605b8fae5f7657e2dec91ca4d79e2b7bad8a0ccd19e00052910",
                  "sha256:a999e900a1b5e177c3c4214362e35d86176ff2dd070b84c49d741d3878715b1c",
                  "sha256:32facc2001182491bc3acdb2d7e98a56dd293f2e5640384d8cfa2ac923176892",
                  "sha256:026f3e39545b6b26f2b38db115821419e7850c4a521bd6ee142bd110a81de17a",
                  "sha256:3fdfbef87fe4370bdca156bacd682562979066bfbf1bf9e74101ef98db706352",
                  "sha256:68afa704e4a86c5c52237edeccdcdba3099dc2d079afde64f4d75009de6c8af1",
                  "sha256:3d4fd95f54f241aab2d00579ced2c7f640e464d6c33dc0a5aef981bb1d437208",
                  "sha256:d184975b84b58732191a46bcb97ad95969d37fa7ce222b9b244d00cde8e9fb2c"
              ]
          },
          "Metadata": {
              "LastTagTime": "0001-01-01T00:00:00Z"
          }
        }
      ]
    ```

## Copying files from or into a running container

- ## `docker cp`
  - we can copy any file or an entire folder from or into a running container
    ```shell
    $ docker cp <local-source-path> <container-id/name>:<destination-path> # copy into running container
    $ docker cp <container-id/name>:<source-path> <local-destination-path> # copy into from container
    ```
  - This command is useful:
    - Add something into a running-container without having to rebuilding the image and restarting the container
      - This is error prone if we forget to copy some files which would break the running application
    - To extract any files (like a bunch of log files) which the application generates, inside the container, outside in the local machine
      ```shell
      $ docker cp <container-id/name>:<source-path-for-log-files> <local-destination-path>
      ```
      - This makes the container, which normally would be a black-box into which we normally can't look directly, becomes more transparent

## Naming & tagging images and containers

- ## `docker run --name` (to name a container)
  ```shell
  $ docker run --name <container-name> <image-id/name>
  CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS      NAMES
  3f854db18e44   6bf8dd2606f7   "docker-entrypoint.s…"   15 seconds ago   Up 14 seconds   3000/tcp   goals
  ```
- ## Image tags

  - Naming an image is called tagging the image
  - Image names have 2 parts: (name:tag)

    1. name: also called a repository of the image
       - defines a specialized group of images with different versions specified by tag part after colon
       - For example node
    2. tag
       - defines a more specialized version of the image
       - For example 14

    - Image names are represented as
      - name:tag (seperated by colon)
      - For example [node:19.5.0](https://hub.docker.com/layers/library/node/19.5.0-buster-slim/images/sha256-8148b6fd4078101b13db8d0d513e9db2255bca379143325ae5f8c90172d39c6a?context=explore)

  - `docker build -t` - To name an image and optionally tag it in the 'name:tag' format, use
    ```shell
    $ docker build -t goals:latest <path-of-dir-with-the-Dockerfile>
    REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
    goals        latest    6bf8dd2606f7   2 hours ago   866MB
    <none>       <none>    d7918fb3abc0   4 weeks ago   866MB
    node         latest    4f35ae175208   6 weeks ago   948MB
    ```
  - `docker tag` command can be used to rename an existing image and create a clone of image with the new name
    ```shell
    $ docker tag goals:latest goals-app:latest
    ```
