# Images & Containers

- Containers are small packages that contain both
  - the application (website, node-server, anything etc)
  - and the environment to run the application
- Container is a running unit of software
  - The thing which runs in the end
- An Image contains the code and the required tools to execute the code, and the container is the running instance of this code
- An Image is created with all the setup instructions and all of our code once, and then we can use this image to create multiple containers(running instances of that image) based-on this image, running on different machines and servers
- An image is in the end the sharable package with all the setup instructions and all of our code

## Creating Images / Finding Existing Images

### Finding Existing Images

- A great source to find pre-built images is [Docker hub](https://hub.docker.com/)
- To run an image (thus create an instance of a container based on an image) locally

```shell
$ docker run <image-name-from-docker-hub>
```

- We could have multiple containers based on same image, running at the same point of time.

  - Could be achieved by simply opening multiple terminals and running the docker-run-image command on each of the terminal

### Creating own Images (Dockerfile)

- Typically we would pull an official base image of the programming language we are using to build an application, from the Docker hub, and then add our code using that pulled image of the programming language, and then execute our code with that pulled image

- Dockerfile

  - Helpful VS Code Extension: [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)

  - Dockerfile is a file containing the instructions for Docker, that we wanna execute when we build abd setup our own custom image

  ```Dockerfile
  <!-- This line says docker to start by first pulling an image named "node" from docker hub -->
  FROM node # <image-name-from-docker-hub>

  <!-- This line says docker about what all files from the local machine should go into the image being created while running this Dockerfile -->
  COPY . /app # COPY <source-path (local path)> <destination-path (path in image)>

  <!-- This line says docker to use /app path from within the image as the working directory for executing further commands below it -->
  WORKDIR /app

  <!-- This line says docker that after copying all the local files to /app, run this command from inside of the image from within the current work dir set from the above command -->
  RUN <cmd>

  <!-- To run a command, not when the image is being built, but only if a container (or multiple containers) is (or are) started (based on this image), use CMD instruction -->
  CMD ["...", "..."] # takes in a list of strings which would make the command we want to be run once a container based on this image is started
  # Example:
  CMD ["node", "server.js"]
  ```

- Docker containers are isolated from the local environment, as a result of which they have their own internal network. Thus, any ports listened upon from within the container are not exposed by default to the local machine. We have to explicitly expose such ports which we know the container would end up listening to once it starts. For that we have `EXPOSE` instruction which takes in a port number from within the container, to be exposed.
- Creating the custom image based on the instructions in Dockerfile:

  ```shell
  $ docker build <path-of-dir-with-the-Dockerfile>
  ```

### Running a container based on custom image

- After running `docker build` command for a Dockerfile with the image-setup instructions, we get a name/id of the created image as output. We use that id to run a container based on that image, using:

  ```shell
  $ docker run <image-id/name>
  ```

### Images are Read-Only

- Once a custom image is built using our source code, if we change the source code post that, we need to rebuild the image to take the updated source code and run containers with updated source code

### Images are layer-based

- each instruction in the Dockerfile is a layer

- While rebuilding an image, only the instruction whose output changed (determined by caching result of every instruction during latest build) and all the instructions following such instruction will be re-evaluated, not all instructions.

- This gives a potential to optimize the Dockerfile such that we would avoid unnecessary re-evaluation of instructions on image rebuilds.

  - Example:

  Before optimizing

  ```Dockerfile
  FROM node

  WORKDIR /app

  COPY . /app

  RUN npm install

  EXPOSE 80

  CMD ["node", "server.js"]
  ```

  After optimizing

  Optimization achieved: We would want to run npm install only when we change something in `package.json` and not anytime else when we just change the source code.

  ```Dockerfile
  FROM node

  WORKDIR /app

  COPY package.json /app

  RUN npm install

  COPY . /app

  EXPOSE 80

  CMD ["node", "server.js"]
  ```
